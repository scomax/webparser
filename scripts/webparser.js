// Script to locate and download webpage text
$(function () {

    $('#get-url').click(function () {

        $('#output').text('');
        var fetchUrl = $('#url-input').val();
        console.log('Fetch Url: ' + fetchUrl);

        fetch(fetchUrl)
            .then(function (response) {
                // When the page is loaded convert it to text
                $('#output').append('Response: ');
                $('#output').append(response);
                return response.text();
            })
            .then(function (html) {
                // Initialize the DOM parser
                var parser = new DOMParser();

                // Parse the text
                var doc = parser.parseFromString(html, "text/html");

                // You can now even select part of that html as you would in the regular DOM 
                // Example:
                // var docArticle = doc.querySelector('article').innerHTML;
                $('#output').append('Parsed document: ');
                $('#output').append(doc);
            })
            .catch(function (err) {
                console.log('Failed to fetch page: ', err);
            });

        /*
        $.get(
            fetchUrl).done(function (data) {
                console.log('Fetch URL completed');
                $('#output').html(data);
            });
        */

    })

    $(document).ajaxSend(function () {
        // raised when a call starts
        $('#status').append('<div>Call started</div>');
    }).ajaxComplete(function () {
        // raised when a call completes
        $('#status').append('<div>Call completed</div>');
    });

})