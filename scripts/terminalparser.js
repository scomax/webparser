console.log('Webparser console program');
// Script to locate and download webpage text
// Based on web scraper example: https://medium.com/@asimmittal/using-jquery-nodejs-to-scrape-the-web-9bb5d439413b

const curl = require("curl");
const jsdom = require("jsdom");
const fs = require("fs")

var finalpage = 646; // Final number of page to be written, based on CRICOS site URL format

// Call self-invoking to determine start point

// Based on self-invoked function: https://scottiestech.info/2014/07/01/javascript-fun-looping-with-a-delay/

(function theLoop(i) {
    setTimeout(function () {

        var urlCode = finalpage - i + 3109;  // Number added is the start number
        const url = "http://cricos.education.gov.au/Institution/InstitutionDetails.aspx?ProviderID=" + urlCode;

        curl.get(url, null, (err, resp, body) => {

            //console.log("Server code: " + resp.statusCode);

            if (resp.statusCode == 200) {
                parseData(body, urlCode);
            }
            else {
                //some error handling
                console.log("Error, page not found, number: " + urlCode);
            }
        });

        if (i % 10 == 0) {
            console.log("Address number: " + urlCode);
        }

        if (--i) {          // If i > 0, keep going
            theLoop(i);       // Call the loop again, and pass it the current value of i
        }
    }, 1500);
})(finalpage);



function parseData(html, counter) {
    //console.log("Enter parseData function");
    const { JSDOM } = jsdom;
    const dom = new JSDOM(html);
    const $ = (require('jquery'))(dom.window);

    //let's start extracting the data
    var organisation = $("#ctl00_cphDefaultPage_institutionDetail_lblInstitutionName").html();
    var contact = $("#ctl00_cphDefaultPage_contactDetail_litPrincipalExecutiveOfficerName").html();
    var address = $("#ctl00_cphDefaultPage_institutionDetail_lblInstitutionPostalAddress").html();
    //console.log("Organisation: " + organisation);
    //console.log("Contact name: " + contact);
    //console.log("Address: " + address);
    writeData(counter, organisation, contact, address);

}

function writeData(counter, org, name, addr) {

    var addressLine = "";
    addressLine = counter + ".|" + org + "|" + name + "|" + addr + "\r\n";

    fs.appendFile('tertiarylist.txt', addressLine, (err) => {
        if (err) throw err;
    });

}
